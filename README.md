`djangoauth` is a djangoapp built using Django 1.11 on Python 2.7. You should have both of those installed.

To use it, simply clone this repo, and `cd` into `djangoauth`. Run `python manage.py runserver`.

The included db.sqlite3 has already had migrations performed, and includes two test users. The passwords are `1q2w3e4r5t`.

They each have associated profiles which can be edited in the user facing area, as well as the admin area.