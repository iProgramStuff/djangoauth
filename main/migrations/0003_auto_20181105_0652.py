# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-11-05 06:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_profile_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='birth_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
