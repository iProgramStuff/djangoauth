from django.conf.urls import url, include
from django.contrib.auth import views as auth_views

from . import views

app_name = "main" # Used for differentiating between different apps on a project (when using {% url %} tag for example)
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^register/$', views.RegisterView.as_view(), name='register'),
    # url(r'^register/$', views.register, name='register'),
    url(r'^login/$', auth_views.LoginView.as_view(), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),
    url(r'^profile/edit$', views.edit_profile, name='edit_profile'),
    (url(r'^profile/(?P<pk>[0-9]+)$', views.ProfileView.as_view(), name='profile'))
]