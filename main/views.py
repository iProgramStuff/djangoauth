# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_list_or_404, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib import messages
from django.views import generic;
from django.http import HttpResponse, HttpResponseRedirect

from .forms import UserForm, ProfileForm

class IndexView(generic.ListView):
	template_name = "main/index.html"
	context_object_name = 'users';
	def get_queryset(self):
		return User.objects.all();

class RegisterView(generic.FormView):
	template_name = 'main/register.html';
	form_class = UserCreationForm;
	success_url = '/';
	def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
		user = form.save()
		messages.success(self.request, 'Your account was successfully created! You can now login')
		return HttpResponseRedirect(self.success_url)

class ProfileView(generic.DetailView):
	model = User;
	template_name = "main/profile.html";
	context_object_name = 'profile_user';

# An alternate, non generic.DetailView profile view method, for my notes.
# def profile(request, user_id):
#	profile_user = get_object_or_404(User, pk=user_id);
#	return render(request, 'profile.html',{'profile_user':profile_user})
#

@login_required
def edit_profile(request):
	if request.method == 'POST':
		user_form = UserForm(request.POST, instance=request.user)
		profile_form = ProfileForm(request.POST, instance=request.user.profile)
		if user_form.is_valid() and profile_form.is_valid():
			user_form.save()
			profile_form.save()
			messages.success(request, 'Your profile was successfully updated!')
			return redirect('main:edit_profile')
		else:
			messages.error(request, 'Please correct the error below.')
	else:
		user_form = UserForm(instance=request.user)
		profile_form = ProfileForm(instance=request.user.profile)
	return render(request, 'main/edit_profile.html', {
		'user_form': user_form,
		'profile_form': profile_form
	})